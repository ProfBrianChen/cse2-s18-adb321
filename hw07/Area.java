//Amanda Baran March 27 2018
//CSE 2 - HW 07 - Write a program that can calculate the area of three different shapes
//

import java.util.Scanner; //imports scanner

public class Area{ //creates class

  public static void main(String[] args){ //creates main method
    
    Scanner myScanner = new Scanner(System.in); //creates scanner
    
    String shape; //declares variable shape
    
    //asks user to enter the name of the shape that the area will be caluculated for
    do{System.out.print("Enter the name of the shape you want to calculate the area for: ");
       while (!myScanner.hasNext()){
         String junkWord = myScanner.next();
         System.out.print("Enter the name of the shape you want to calculate the area for: ");
       } shape = myScanner.next();  
    } while (!shape.equals("rectangle") && !shape.equals("triangle") && !shape.equals("circle"));
    
    //asks user for dimensions of shape based on type of shape they wrote and prints out the area 
    if (shape.equals("rectangle")){
      System.out.print("Enter the length of the rectangle: ");
       while (!myScanner.hasNextDouble()){
         String junkWord = myScanner.next();
         System.out.print("Enter the length of the rectangle: ");
       } double length = myScanner.nextDouble();
      
      System.out.print("Enter the width of the rectangle: ");
       while (!myScanner.hasNextDouble()){
         String junkWord = myScanner.next();
         System.out.print("Enter the width of the rectangle: ");
       } double width = myScanner.nextDouble();
      
      double area = rectangle(width, length);
      System.out.println("The area of the rectangle is " + area);  
    }
    
    else if (shape.equals("triangle")){
      System.out.print("Enter the length of the base of the triangle: ");
       while (!myScanner.hasNextDouble()){
         String junkWord = myScanner.next();
         System.out.print("Enter the length of the base of the triangle: ");
       } double base = myScanner.nextDouble();
      
      System.out.print("Enter the height of the triangle: ");
       while (!myScanner.hasNextDouble()){
         String junkWord = myScanner.next();
         System.out.print("Enter the height of the triangle: ");
       } double height = myScanner.nextDouble();
      
      double area = triangle(base, height);
      System.out.println("The area of the triangle is " + area); 
    }
    
    else if (shape.equals("circle")){
      System.out.print("Enter the radius of the circle: ");
       while (!myScanner.hasNextDouble()){
         String junkWord = myScanner.next();
         System.out.print("Enter the radius of the circle: ");
       } double radius = myScanner.nextDouble();
      
        double area = circle(radius);
      System.out.println("The area of the circle is " + area); 
    }
 }

  //methods for calculating areas of the different shapes
  public static double rectangle(double width, double length){ 
    return(width*length);
  }
  
  public static double triangle(double height, double base){
    return(.5 * height * base);
  }
  
  public static double circle(double radius){
    return(Math.PI * radius * radius);
  }
  
}  


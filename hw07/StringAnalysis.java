//Amanda Baran March 27 2018
//CSE 2 - HW 07 - Write a program that examines characters in a string
//

import java.util.Scanner;

public class StringAnalysis{
  
  // METHOD STRING AND INTEGERS
  public static void inputString(String string, int size){
    //checks the characters in the string to check if it's a letter
    for (int i = 1; i <= size; i++){
      if (string.charAt(i) >= 'a' && string.charAt(i) <= 'z'){
        System.out.println(string.charAt(i) + " is a letter.");
      }
      else{
        System.out.println(string.charAt(i) + "is not a letter.");
      }
    }
    
     
  }
  
  
  // METHOD STRING
  public static void inputString(String string){
    
    int length = string.length();
    //checks the characters in the string to check if it's a letter
    for (int i = 1; i < length; i++){
      if (string.charAt(i) >= 'a' && string.charAt(i) <= 'z'){
        System.out.println(string.charAt(i) + " is a letter");
      }
      else{
        System.out.println(string.charAt(i)  + " is not a letter.");
      }
    }
    
  }
  
  
  
  // MAIN METHOD
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    String string = "abcdef";
    int choice = 1;
    
    //prompts user to enter the string they want to be evaluated
    System.out.print("Enter the string you would like to analyze: ");
    while (!myScanner.hasNext()){
          String junkWord = myScanner.next();
          System.out.print("Enter the string you would like to analyze: ");
    } string = myScanner.nextLine();
    
    //prompts user to enter if they want the whole string to be evaluated
    do{System.out.print("Would you like to choose how many characters of the string you want to analyze? Enter 1 for yes, 2 for no: ");
       while (!myScanner.hasNextInt()){
       String junkWord = myScanner.next();
       System.out.print("Would you like to choose how many characters of the string you want to analyze? Enter 1 for yes, 2 for no: ");
      } choice = myScanner.nextInt();
    } while (choice < 1 || choice > 2);
    
   int size = 0; 
    //prompts the user to enter how many characters they want analyzed
  if (choice == 1){
    System.out.print("Enter how many characters of the string you want to analyze: ");
    while(!myScanner.hasNextInt()){
      String junkWord = myScanner.next();
      System.out.print("Enter how many characters of the string you want to analyze: ");
    } size = myScanner.nextInt();
    
    inputString(string, size); //call method
  }
    else if (choice == 2){
      inputString(string); //call method
    }
    
  }
  
}

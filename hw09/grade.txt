Grade:   70/100

Comments:

A) Does the code compile?  How can any compiler errors be resolved?

	Code compiles

B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?

	No runtime errors

C) How can any runtime errors be resolved?

	N/A

D) What topics should the student study in order to avoid the errors they made in this homework?

	N/A

E) Other comments:

	Only gives numbers 0-51 for each player 


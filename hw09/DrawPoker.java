//Amanda Baran April 17 2018
//CSE2 - HW 09- Practice manipulating arrays and writing methods that have array parameters.

import java.util.Random; 

public class DrawPoker{
  
  public static void main(String[] args){
    
    int[] index;
    index = new int[52];
    
  
    for(int i = 0; i < 52; i++){
      index[i] = i;
    
      
      //determines suit of card
      String suit = "abc";
      if(index[i] < 13){
        suit = "diamond";
      }
      else if (index[i] > 12 && index[i] < 26){
        suit = "clubs";
      }
      else if(index[i] > 25 && index[i] < 39){
        suit = "hearts";
      }
      else{
        suit = "spades";
      }
      
      //detemines card number 
      int number = 0;
      switch (index[i] % 13) {
        case 0:
          number = 1;
          break;
        case 1:
          number = 2;
          break;
        case 2:
          number = 3;
          break;
       case 3:
         number = 4;
         break;  
       case 4:
         number = 5;
         break;
       case 5:
         number = 6;
         break;
       case 6:
         number = 7;
         break;  
       case 7:
         number = 8;
         break;
       case 8:
         number = 9;
         break;  
       case 9:
         number = 10;
         break; 
       case 10:
         number = 11;
         break;
       case 11:
         number = 12;
         break;
       case 12:
         number = 13;
         break;
      }
      
   }
   
   int[] array = new int[10]; 
   array = shuffle(index, 52); //shuffles index
    
   int[] handOne = new int [5];
   int[] handTwo = new int [5];
      
    /*
   for(int i = 0; i < 10; i++){ //deals shuffled cards two hands 
     System.out.print(index[i] + " ");
   }  
    System.out.println();
    */
   for (int l = 0; l < 10; l++){ 
     if(l % 2 == 0){
       handOne[l/2] = array[l];
     }
     else{
       handTwo[l/2] = array[l];
     }
   } 
  
   System.out.println("Player One's Hand is: ");
   for(int j = 0; j < 5; j++){ //prints out hands
     System.out.print(handOne[j] + " ");
   }
   System.out.println();
   System.out.println("Player Two's Hand is: "); 
   for(int k =0; k < 5; k++){
     System.out.print(handTwo[k] + " ");
   }
   System.out.println();
    
 }
  
  /*  public static boolean pair(int[] handOne, int[] handTwo){
      for(i =0; i < 5; i++){
        if (handOne[i] == handOne [i+2]){
          System.out.println("Player Two has a Pair");
        }
      }
      for(i =0; i < 5; i++){
        if (handTwo[i] == handTwo [i+2]){
          System.out.println("Player Two has a Pair");
        }
      }
    }
  */
    public static int[] shuffle(int[] deck, int n){ //shuffles deck
      
      Random randomNumber = new Random();
     
      for(int i =0; i < n; i++){
        int r = i + randomNumber.nextInt(52 - i);
      
        int temp = deck[r];
        deck[r] = deck[i];
        deck[i] = temp;
      
      }
      return deck;
  
    }
  
  
      
  
}
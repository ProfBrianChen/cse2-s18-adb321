//Amanda Baran February 10 2018 CSE 2
//HW 03 Pyramid
//Creare a program that calculates the volume of a pyramid.
//
import java.util.Scanner;

public class Pyramid{
  //main method
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner ( System.in );
    // tells scanner that I am creating an instance that will take input from STDIN
    
    System.out.print("The square side of the pyramid is (input length) ");
    double Length = myScanner.nextDouble ();
    
    System.out.print("The height of the pyramid is (input height) : ");
    double Height = myScanner.nextDouble ();
    
    double Volume = (Length * Length * Height)/3;
    //equation for volume of a pyramid
    
    System.out.println("The volume inside the pyramid is " + Volume);
   
      
  } //end of main method
} //end of class
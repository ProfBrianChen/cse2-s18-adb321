// Amanda Baran February 10, 2018 CSE 2
//HW 03- Convert
// Displays the quantity of cubic miles affected by hurricaine precipitation.
//

import java.util.Scanner;

public class Convert{
  //main method
  
public static void main(String[] args) {
    
    Scanner myScanner = new Scanner ( System.in );
   // tells scanner that I am creating an instance that will take input from STDIN
  
  System.out.print("Enter the affected area in acres (in the form xx.xx): ");
  double areaAcres = myScanner.nextDouble();
  
  System.out.print("Enter the inches of rainfall in the affected area (in the form xx): ");
  double rainInches = myScanner.nextDouble();
  
  double cubicMiles;
   
  cubicMiles = areaAcres * 43560 * rainInches / 12 * 6.79357e-12;
  //equation to calculate how many cubic miles were affected
  
  
  System.out.println(  cubicMiles + " cubic miles were affected.");
  
} //end of main method
    
} //end of class

//Amanda Baran February 18 2018 CSE 2
//HW 04 Perform a random roll of dice and score the roll based on the game of Yahtzee.
//
import java.util.Scanner;

public class Yahtzee{
  
  public static void main(String[] args) {
    //main method required for every java program
    
    Scanner myScanner = new Scanner ( System.in );
    
    System.out.print("Would you like to enter your own dice values? Enter 1 for Yes or 2 for No: ");
    int choice = myScanner.nextInt();
    //asks user if they want to use their own values or use randomly generated values
    
    int die1 =0;
    int die2 =0;
    int die3 =0; 
    int die4 =0; 
    int die5 =0;
    
    if(choice < 1 || choice > 2){
       System.out.print("Error. Invalid input.");
      System.exit(0);
      }
    //sends error messages if user doesn't answer correctly
    
    else if(choice == 1){   
      System.out.print("Enter value for die 1 between 1 and 6: ");
      die1 = myScanner.nextInt();
      if (die1 > 6 || die1 < 1){
        System.out.print("You entered an invalid number.");
        System.exit(0);
      }
      System.out.print("Enter value for die 2 between 1 and 6: ");
      die2 = myScanner.nextInt();
      if (die2 > 6 || die2 < 1){
        System.out.print("You entered an invalid number.");
        System.exit(0);
      }
      System.out.print("Enter value for die 3 between 1 and 6: ");
      die3 = myScanner.nextInt();
      if (die3 > 6 || die3 < 1){
        System.out.print("You entered an invalid number.");
        System.exit(0);
      }
      System.out.print("Enter the value for die 4 between 1 and 6: ");
      die4 = myScanner.nextInt();
      if (die4 > 6 || die4 < 1){
        System.out.print("You entered an invalid number.");
        System.exit(0);
      }
      System.out.print("Enter the value for die 5 between 1 and 6: ");
      die5 = myScanner.nextInt();
      if (die5 > 6 || die5 < 1){
        System.out.print("You entered an invalid number.");
        System.exit(0);
      }
      //collects values of each die from user. sends error message if entry is invalid.
      
      System.out.println("Your roll is " + die1 + " " + die2 + " " + die3 + " " + die4 + " " + die5);
      //prints out the roll that the user picked 
    }
    
    else if(choice == 2){
      die1 = (int)(Math.random()*6)+1;
      die2 = (int)(Math.random()*6)+1;
      die3 = (int)(Math.random()*6)+1;
      die4 = (int)(Math.random()*6)+1;
      die5 = (int)(Math.random()*6)+1;
      System.out.println("The random roll is " + die1 + " " + die2 + " " + die3 + " " + die4  + " "+ die5 );
      //generates 5 different random numbers that represent a number on a die
    }
    
    int countAce = 0;
    int countTwo = 0;
    int countThree = 0;
    int countFour = 0;
    int countFive = 0;
    int countSix = 0;
    
    int scoreAce;
    int scoreTwo;
    int scoreThree;
    int scoreFour;
    int scoreFive; 
    int scoreSix;
   
    if (die1 == 1){
      countAce++;
    }
    if (die2 == 1){
      countAce++;
    }
    if (die3 == 1){
      countAce++;
    }
    if (die4 == 1){
      countAce++;
    }
    if (die5 == 1){
      countAce++;
    }
    
    scoreAce = countAce * 1;
    System.out.println("Your score for Aces is " + scoreAce );
    //counts and prints out score of Aces 
    
    
    if (die1 == 2){
      countTwo++;
    }
    if (die2 == 2){
      countTwo++;
    }
    if (die3 == 2){
      countTwo++;
    }
    if (die4 == 2){
      countTwo++;
    }
    if (die5 == 2){
      countTwo++;
    }
    
    scoreTwo = countTwo * 2;
    System.out.println("Your score for Twos is " + scoreTwo );
    //counts and prints out score of Twos
    
    if (die1 == 3){
      countThree++;
    }
    if (die2 == 3){
      countThree++;
    }
    if (die3 == 3){
      countThree++;
    }
    if (die4 == 3){
      countThree++;
    }
    if (die5 == 3){
      countThree++;
    }
    
    scoreThree = countThree * 3;
    System.out.println("Your score for Threes is " + scoreThree );
//counts and prints out score of Threes
    
    if (die1 == 4){
      countFour++;
    }
    if (die2 == 4){
      countFour++;
    }
    if (die3 == 4){
      countFour++;
    }
    if (die4 == 4){
      countFour++;
    }
    if (die5 == 4){
      countFour++;
    }
    
    scoreFour = countFour * 4;
    System.out.println("Your score for Fours is " + scoreFour );
    //counts and prints out score of Fours
    
    if (die1 == 5){
      countFive++;
    }
    if (die2 == 5){
      countFive++;
    }
    if (die3 == 5){
      countFive++;
    }
    if (die4 == 5){
      countFive++;
    }
    if (die5 == 5){
      countFive++;
    }
    
    scoreFive = countFive * 5;
    System.out.println("Your score for Fives is " + scoreFive );
    //counts and prints out score of Fives
    
    if (die1 == 6){
      countSix++;
    }
    if (die2 == 6){
      countSix++;
    }
    if (die3 == 6){
      countSix++;
    }
    if (die4 == 6){
      countSix++;
    }
    if (die5 == 6){
      countSix++;
    }
    
    scoreSix = countSix * 6;
    System.out.println("Your score for Sixes is " + scoreSix );
    //counts and prints out score of Sixes
    
    int totalUpperInitial = scoreAce + scoreTwo + scoreThree + scoreFour + scoreFive + scoreSix;
    int totalUpperScore = 0;
    
    if (totalUpperInitial >= 63){
      totalUpperScore = totalUpperInitial + 35;
      System.out.println("Your total score for the upper section is " + totalUpperScore);
    }
    else if (totalUpperInitial < 63){
      totalUpperScore = totalUpperInitial;
      System.out.println("Your total score for the upper section is " + totalUpperScore);
    }
    //Prints out total score of upper section
    
    int lower =0;
 
    if (countAce ==5 || countTwo ==5 || countThree ==5 || countFour ==5 || countFive ==5 || countSix ==5){
      System.out.println("Yahtzee! That's 50 points!");
      lower = 50;
    } //prints out if user rolled a yahtzee
    
    else if ((countAce ==1 && countTwo ==1 && countThree ==1 && countFour ==1 && countFive ==1) ||
             (countTwo ==1 && countThree ==1 && countFour ==1 && countFive ==1 && countSix ==1)){
      System.out.println("You rolled a large straight. That's 4 points!");
      lower = 40;
    } //prints out if user rolled a large straight
    
    else if ((countAce ==2 && countTwo ==1 && countThree ==1 && countFour ==1) ||
             (countAce ==1 && countTwo ==2 && countThree ==1 && countFour ==1) ||
             (countAce ==1 && countTwo ==1 && countThree ==2 && countFour ==1) ||
             (countAce ==1 && countTwo ==1 && countThree ==1 && countFour ==2) ||
             (countAce ==1 && countTwo ==1 && countThree ==1 && countFour ==1 && countSix ==1) ||
             (countTwo ==2 && countThree ==1 && countFour ==1 && countFive ==1) ||
             (countTwo ==1 && countThree ==2 && countFour ==1 && countFive ==1) ||
             (countTwo ==1 && countThree ==1 && countFour ==2 && countFive ==1) ||
             (countTwo ==1 && countThree ==1 && countFour ==1 && countFive ==2) ||
             (countThree ==2 && countFour ==1 && countFive ==1 && countSix ==1) ||
             (countThree ==1 && countFour ==2 && countFive ==1 && countSix ==1) ||
             (countThree ==1 && countFour ==1 && countFive ==2 && countSix ==1) ||
             (countThree ==1 && countFour ==1 && countFive ==1 && countSix ==2) ||
             (countThree ==1 && countFour ==1 && countFive ==1 && countSix ==1 && countAce ==1)){
      System.out.println("You rolled a small straight. That's 30 points!");
      lower = 30;
    } //prints out if user rolled a small straight
    
    else if ((countAce==3 || countTwo ==3 || countThree ==3 || countFour ==3 || countFive ==3 || countSix ==3) && 
             ((countAce==2 || countTwo ==2 || countThree ==2 || countFour ==2 || countFive ==2 || countSix ==2))){
      System.out.println("You rolled a full house. That's 25 points!");
      lower = 25;
    } //prints out if user rolled a full house
    
    else if (countAce==3 || countTwo ==3 || countThree ==3 || countFour ==3 || countFive ==3 || countSix ==3){
      System.out.println("You rolled a three of a kind. That's " + lower + "points!");
      lower = die1 + die2 + die3 + die4 + die5;
    } //prints out if user rolled a three of a kind
    
    else if (countAce ==4 || countTwo ==4 || countThree ==4 || countFour ==4 || countFive ==4 || countSix ==4){
      System.out.println("You rolled a four of a kind. That's " + lower + "points!");
      lower = die1 + die2 + die3 + die4 + die5;
    } //prints out if user rolled a four of a kind
    
    else {
      lower = die1 + die2 + die3 + die4 + die5;
      System.out.println("You rolled a chance. That's " + lower + " points!");
    } //prints out if user rolled a chance
    
    System.out.println("Your total score of the lower section is " + lower );
    //prints out total of lower section
    
    int grandTotal = totalUpperScore + lower;
    
    System.out.println("Your grand total is " + grandTotal);
    //prints out grand total for roll including upper and lower section
    
      
    }
       
    
  }
    
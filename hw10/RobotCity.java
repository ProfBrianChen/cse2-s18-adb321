//Amanda Baran April 21 2018
//CSE 2 HW 10- Practice with multidimensional arrays.

import java.util.Random;

public class RobotCity{
  
  public static void main(String[] args){ //main method
   
    System.out.println("Original City: ");
    System.out.println();
     
    int[][] city;
    city = buildCity();
    display(city); //prints out the original city
    
    System.out.println();
    System.out.println("Invaded City: ");
    System.out.println();
    
    int k = (int)(Math.random() * 10) + 5;
    int[][] invadedCity;
    invadedCity = invade(city, k);
    display(invadedCity); //prints out the invaded city
    
    System.out.println();
    System.out.println("Update: ");
    System.out.println();
    
    for(int i = 0; i < 5; i++){ //prints out the updated city 5 times
    int[][] updatedCity;
    updatedCity = update(invadedCity);
    display(updatedCity);
    System.out.println();
      
    }
  }
  
  public static int[][] buildCity(){ //method that builds the original city by making an array of a random size
    
    int columns = (int)(Math.random() * 6) + 10;
    int rows = (int)(Math.random() * 6) + 10;
    
    int[][] cityArray = new int[rows][columns];
    
    for(int i = 0; i < rows; i++){
      for(int j = 0; j < columns; j++){
      cityArray[i][j] = (int)(Math.random()*900) + 100;
      }
    }  
    
    return cityArray;
  } 
  
  public static void display(int[][] cityArray){ //method that prints the array
    
    for(int i = 0; i < cityArray.length; i++){
      for(int j = 0; j < cityArray[i].length; j++){
        System.out.printf("%5s ", cityArray[i][j]);
      }
      System.out.println();
    }
    
    
  }
  
  public static int[][] invade(int[][] cityArray, int k){ //method that puts negatives into the array to represent robots
    
    int rows = cityArray.length; 
    int columns = cityArray[0].length;
    int[][] array = new int[rows][columns];
    

    
    for(int i = 0; i < k; i++){ //plants the robots in city
      int randomRow = (int)(Math.random() * rows);
      int randomColumn = (int)(Math.random() * columns);
      
      while(array[randomRow][randomColumn] ==  1){
        randomRow = (int)(Math.random() * rows);
        randomColumn = (int)(Math.random() * columns);
      }
      array[randomRow][randomColumn] = 1;
      
      int value = cityArray[randomRow][randomColumn];
      value = -value;
      cityArray[randomRow][randomColumn] = value;
    }
    return cityArray;
    
  }
  
  public static int[][] update(int[][] cityArray){ //updates the invaded array to move robots towards east
    
    int rows = cityArray.length; 
    int columns = cityArray[0].length;
    
    for(int i = 0; i < rows; i++){ //moves negative values to left
      for(int j = (columns -1); j >= 0; j--){
        int value = cityArray[i][j];
        if(value < 0){
          value = -value;
          cityArray[i][j] = value;
          if(j < columns -1){
            int newValue = cityArray[i][j+1];
            newValue = -newValue;
            cityArray[i][j+1] = newValue;
          }
        }
        
      }
    }
    
    return cityArray;
    
  }
  
}
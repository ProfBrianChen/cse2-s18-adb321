Grade:     100/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
yes
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error
?
No
C) How can any runtime errors be resolved?
N/A
D) What topics should the student study in order to avoid the errors they made in this homework?
None
E) Other comments:
Great job!
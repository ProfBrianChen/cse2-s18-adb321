//Amanda Baran April 9 2018
//CSE 2 HW 08- Practice with arrays and searching single dimensional arrays.

import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class CSE2Linear{ //class
  
public static void main(String[] args){ //main method
  
  int[] finalGrades;
  finalGrades = new int[15];
  
  Scanner myScanner = new Scanner (System.in);
  System.out.println("Enter 15 integers for students' final grades in CSE2:");
  
  //checks to make sure user inputs are valid
  for(int i = 0; i < finalGrades.length; i++){
    while(!myScanner.hasNextInt()){
      String junkword = myScanner.next();
      System.out.print("Input is not an integer. Enter 15 integers: ");
    } finalGrades[i] = myScanner.nextInt();
    
    while(finalGrades[i] > 100 || finalGrades[i] < 0){
      System.out.print("Input is not within range. Enter 15 integers between 0 and 100: ");
      finalGrades[i] = myScanner.nextInt();
    }
    
    if(i>0){
    while(finalGrades[i] <= finalGrades[i-1]){
      System.out.print("Input too small. Enter an integer greater than the last input: ");
      finalGrades[i] = myScanner.nextInt();
    }
    }
  } 
  
  
  System.out.println(Arrays.toString(finalGrades));
  
  //prompts user for grade they want to search for and prints if it was found
  Scanner myScanner2 = new Scanner(System.in);
  System.out.println("Enter the grade you want to search for: ");
  int grade = myScanner2.nextInt();
  
  if(binarySearch(finalGrades, grade)){
    System.out.println( grade + " was found."); 
  }
  else{
    System.out.println(grade + " was not found.");
  }
  
  randomScrambling(finalGrades, grade);
  
}
  
  public static void linearSearch(int[] list, int grades){ //linear search method
    Scanner myScanner3 = new Scanner(System.in);
    System.out.println("Enter a grade to search for: ");
    grades = myScanner3.nextInt();
    
    for(int i = 0; i < list.length; i++){
      if(list[i] == grades){
        System.out.println( grades + " was found.");
        return;
      }
      else{
        System.out.println(grades + " was not found.");
      }
    }    
  }
  
  public static boolean binarySearch(int[] list, int grade){ //binary search method
    int first = 0;
    int last = list.length - 1;
    int mid = (first + last)/2;
    int iteration = 1;
    
    while (first <= last){
      if(list[mid] < grade){
        first = mid + 1;
      }
      else if (list[mid] == grade){
        return true;
      }
      else{
        last = mid - 1;
      }
      iteration++;
      mid = (first+last)/2;
    }
    return false;
  }
  
  public static void randomScrambling(int[] list, int grade){ //random scrambling method
    System.out.println();
    System.out.println("Numbers scrambled randomly: ");
    
    for(int i = 0; i < list.length; i++){
      int index = (int)(Math.random() * list.length);
      int x = list[i];
      
      list[i] = list[index];
      list[index] = x;
    }
    
    for(int y: list){
      System.out.println(y);
    }
    
    linearSearch(list, grade);
  }
  
}
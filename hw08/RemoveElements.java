import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
    System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
	
	public static int[] randomInput(){
		
		int[] array;
		array = new int[10];
		
		for(int i = 0; i < 10; i++){
			int number = (int)(Math.random()*10);
			array[i] = number;
		}
		return array;
		
	}
	
	public static int[] delete(int[] list, int pos){
		int[] array1;
		array1 = new int[list.length -1];
		
		if(pos > 9 || pos < 0){
			System.out.println("The indext is out of bounds.");
			return list;
		}
		
		for(int i = 0; i < list.length - 1; i++){
			if(i < pos){
				array1[i] = list[i];
			}
			else if (i >= pos){
				array1[i] = list[i+1];
			}
		}
		return array1;
	}
	
	public static int[] remove(int[] list, int target){
		
		int counter =0;
		for(int j = 0; j<= list.length; j++){
			if(list[j] == target){
				counter++;
			}
		}
		
		if(counter == 0){
			System.out.println("The target element was not found.");
			return list;
		}
		
		else{
			System.out.println("The target element was found.");
	
			int[] array2;
			array2 = new int[list.length- counter];
		
			int k = 0;
			for(int i = 0; i <= list.length - counter; i++){
				if(list[i+k] == counter){
					++k;
					if(list[i+k] == counter){
						++k;
						if(list[i+k] == counter){
							++k;
							if(list[i+k] == counter){
								++k;
							}
						}
					}
				}
			array2[i] = list[i+k];
		
			}
	
		return array2;
	
		}
	}
}

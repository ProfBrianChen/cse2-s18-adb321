//Amanda Baran April 6 2018
//CSE 2 Lab 08 -- Create one-dimmensional arrays in regards to students and their grades.
//

import java.util.Scanner;

public class lab08{
  public static void main (String[] args){
    
    //creates array for student names
    int numStudents = (int)(Math.random()*6)+5;  
    String[] students;
    students = new String[numStudents];
    
    //creates array for midterm grades
    int grade = 0;
    int[] midterm = new int [numStudents];
    
    //asks user to enter names of students
    System.out.println("Enter " + numStudents + " names of students: ");
    Scanner myScanner = new Scanner (System.in);
    
    //fills in arrays
    for(int i = 0; i < numStudents; i++){
      students[i] =  myScanner.next();
      grade = (int)(Math.random()*101);
      midterm[i] = grade;
    }
     
    //prints out grades for the names entered
    System.out.println("Here are the grades of the students entered above: ");
    for(int i = 0; i < numStudents; i++){
     System.out.println(students[i] + ":" + midterm[i]);
    }
    
  }
}
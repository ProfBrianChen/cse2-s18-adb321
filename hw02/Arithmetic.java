/////////////////////////////////////
/// Amanda Baran February 3 2018 CSE 2 - 210 ///
/// HW 02 // This program will calculate the total price, sales tax, and costs of items bought at a store.//
///
public class Arithmetic {

  public static void main(String[] args) {
    // Assumptions 
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts= 1;
    //cost per belt
    double beltPrice = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    //Variables
    
    double totalCostofPants;  //total cost of pants
    double totalCostofShirts;  //total cost of sweatshirts
    double totalCostofBelts;  //total cost of belts
    double totalCostofPantspaSalesTax;  //total cost of sales tax on pants
    double totalCostofShirtspaSalesTax;  //total cost of sales tax on sweatshirts
    double totalCostofBeltspaSalesTax;  //total cost of sales tax on belts
    double totalCostofPurchases;  //total cost of all items before tax
    double totalpaSalesTax;  //total cost of sales tax
    double totalCostofTransaction;  //total cost paid with tax 
    
    //Calculations
    
    totalCostofPants=numPants*pantsPrice;  
    totalCostofShirts=numShirts*shirtPrice;
    totalCostofBelts=numBelts*beltPrice;
    totalCostofPantspaSalesTax=totalCostofPants*paSalesTax;
    totalCostofShirtspaSalesTax=totalCostofShirts*paSalesTax;
    totalCostofBeltspaSalesTax=totalCostofBelts*paSalesTax;
    totalCostofPurchases=totalCostofPants+totalCostofShirts+totalCostofBelts;
    totalpaSalesTax=totalCostofPantspaSalesTax+totalCostofShirtspaSalesTax+
      totalCostofBeltspaSalesTax;
    totalCostofTransaction=totalCostofPants+totalCostofShirts+totalCostofBelts+
      totalpaSalesTax;
    
    //Print out the output data.
    
    
    System.out.println("The total cost of pants is $"+totalCostofPants);
    System.out.println("The total cost of sweatshirts is $"+totalCostofShirts);
    System.out.println("The total cost of belts is $"+totalCostofBelts);
    //Above displays costs of each item.
    
    System.out.println("The total sales tax charged on pants is $"+
                       String.format("%.2f", totalCostofPantspaSalesTax));
    System.out.println("The total cost of sales tax charged on sweatshirts is $"+
                      String.format("%.2f", totalCostofShirtspaSalesTax));
    System.out.println("The total cost of sales tax charged on belts is $"+
                      String.format("%.2f", totalCostofBeltspaSalesTax));
     //Above displays sales tax charged on each item.                  
                       
    System.out.println("The total cost of purchases before tax is $"+
                      totalCostofPurchases); 
    //Above displays total cost of purchases, not including sales tax.                  
                       
    System.out.println("The total cost of sales tax is $"+String.format("%.2f", totalpaSalesTax));
    //Above displays total cost of the sales tax charged on all items.
                       
    System.out.println("The total paid for this transaction, including sales tax is $"+
                      String.format("%.2f", totalCostofTransaction));
    //Above displays total cost of the transaction, including sales tax.
   
     
  }
}
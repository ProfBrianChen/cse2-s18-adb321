/////////////////////////////////////////
/// Amanda Baran January 30 2018 CSE 2 ///
///
public class WelcomeClass {
  
  public static void main(String[] args) {
    // Prints "Welcome" to the terminal window.
    System.out.println("    -----------");
    System.out.println("    | WELCOME | ");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\" );
    System.out.println("<-A--D--B--3--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    
    System.out.println("My name is Amanda Baran. I am a freshman living in Dravo and majoring in Finance and Accounting, with a possible minor in Computer Science.");
                     
    
  }
}
//Amanda Baran April 13 2018
//CSE 2 - This lab will illustrate the effect of passing arrays as method arguments, and how the array arguments are affected by the call by Value nature.
//

public class lab09{
  
  public static int[] copy(int[] input){
    
    int[] output = new int[input.length];
    
    for(int i = 0; i < input.length; i++){
      output[i] = input[i];
    }
    
     return output;   
  }
  
  
  public static void inverter(int[] invertArray){
    
    for(int i = 0; i < (invertArray.length / 2); i++){
      int temp = invertArray[i];
      invertArray[i] = invertArray[invertArray.length - i - 1];
      invertArray[invertArray.length - 1 - i] = temp;
    }
    
  }

  public static int[] inverter2(int[] newArray){
    
    int[] newCopy = copy(newArray);
    
    for(int i = 0; i < (newCopy.length / 2); i++){
      int temp = newCopy[i];
      newCopy[i] = newCopy[newCopy.length - i - 1];
      newCopy[newCopy.length - 1 - i] = temp;
    }
    
    
    return newCopy;
    
  }
  
  public static void print(int[] array){
    
    for(int i = 0; i < array.length; i++){
      System.out.print(array[i]);
    }
    System.out.println();
  }
  
  public static void main(String[] args){
    
    int[] array0 = {1,2,3,4,5,6,7,8,9};
    for(int i = 0; i < 9; i++){
      System.out.print(array0[i]);
    }
    System.out.println();
    
    
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    
    inverter(array0);
    print(array0);
  
    inverter2(array1);
    print(array1);
    
    int[] array3 = inverter2(array2); 

    print(array3);
    
    
  }
  
  
}


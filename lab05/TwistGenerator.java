//Amanda Baran March 2 2018 CSE 2
//Lab 05- Prompts the user for the length of a twist and prints out a twist of that length.
//
import java.util.Scanner; //imports scanner

public class TwistGenerator{ //creates class
  
  public static void main(String[] args){ //main method
    
    Scanner myScanner = new Scanner(System.in); //creates scanner
    
    
    int length = 1; //initializes the length
    
    do {System.out.print("Enter your desired length of the twist: "); 
        while (!myScanner.hasNextInt()){
          String junkWord = myScanner.next();
          System.out.print("Enter your desired length of the twist: ");
        } 
        length = myScanner.nextInt(); 
       } 
    while (length < 0); 
    // prompts user for length of twist until they correctly enter a positive integer
    
    int numberOfXs = (length +1)/3; //formula to find number of Xs based on length entered
    int topSlash = (length / 3); //formula to find number of top slash patterns based on length entered
    int bottomSlash = (length / 3); //formula to find number of bottom slash patterns based on length entered
    
    int a = 0;
    int b = 0;
    int c = 0;
    int d = 0;
    int e = 0;
    int f = 0;
    int g = 0;
   // initializes all variables used below
    
    if (length % 3 == 0){
      while (topSlash > a){
        System.out.print("\\ /");
        ++a;
      }
    }
    else if (length % 3 == 1){
      while (topSlash > b){
        System.out.print("\\ /");
        ++b;
      }
      System.out.print("\\");
    }
    else if (length % 3 == 2){
      while (topSlash > c){
        System.out.print("\\ /");
        ++c;
      }
      System.out.print("\\");
    }
    System.out.println("");
    // prints out the correct number of slashes for top line based on length
    
    while (numberOfXs > d){
      System.out.print(" X ");
      ++d;
    }
    System.out.println("");
    // prints out the correct number of Xs based on length
    
    if (length % 3 == 0){
      while (bottomSlash > e){
        System.out.print("/ \\");
        ++e;
      }
    }
    else if (length % 3 == 1){
      while (bottomSlash > f){
        System.out.print("/ \\");
        ++f;
      }
      System.out.print("/");
    }
    else if (length % 3 == 2){
      while (bottomSlash > g){
        System.out.print("/ \\");
        ++g;
      }
      System.out.print("/");
    }
    System.out.println("");
    // prints out the correct number of slashes for bottom line based on length
      
      
      
    } //end of main method
    
    
  } //end of class

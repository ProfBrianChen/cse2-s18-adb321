//Amanda Baran March 14 2018 
//CSE 2 - HW 06
// Write a program that displays an argyle pattern in text

import java.util.Scanner; //imports scanner

public class Argyle{ //creates class
  public static void main(String[] args){ //creates main method
    
    Scanner myScanner = new Scanner(System.in); //creates scanner
    
    //gets width of veiwing window from user
    int widthWindow = 1;
    do {System.out.print("Enter a positive integer to represent the width of the viewing window: ");
        while (!myScanner.hasNextInt()){
          String junkWord = myScanner.next();
          System.out.print("Enter a positive integer to represent the width of the viewing window: ");
        } 
        widthWindow = myScanner.nextInt();
       }
    while (widthWindow < 0); 
   
    //gets height of veiwing window from user
   int heightWindow = 1;
   do {System.out.print("Enter a positive integer to represent the height of the viewing window: ");
        while (!myScanner.hasNextInt()){
          String junkWord = myScanner.next();
          System.out.print("Enter a positive integer to represent the height of the viewing window: ");
        } 
        heightWindow = myScanner.nextInt();
       }
    while (heightWindow < 0);  
    
    //gets size of argyle diamonds from user
    int sizeDiamonds = 1;
    do {System.out.print("Enter a positive integer to represent the size of the argyle diamonds: ");
        while (!myScanner.hasNextInt()){
          String junkWord = myScanner.next();
          System.out.print("Enter a positive integer to represent the size of the argyle diamonds: ");
        } 
        sizeDiamonds = myScanner.nextInt();
       }
    while (sizeDiamonds < 0); 
    
    //gets width of stripe from user
    int stripeWidth = 1;
    do {System.out.print("Enter a positive  odd integer less than half of the size of the diamonds to represent the width of the argyle stripe: ");
        while (!myScanner.hasNextInt()){
          String junkWord = myScanner.next();
          System.out.print("Enter a positive integer to represent the width of the argyle stripe: ");
        } 
        stripeWidth = myScanner.nextInt();
       }
    while (stripeWidth < 0 || (stripeWidth % 2 == 0) || stripeWidth > (sizeDiamonds/2)); 
    
    //gets character to fill diamonds with from user
    System.out.print("Enter your first character to fill the diamonds with: ");
    String temp1 = myScanner.next();
    char fillOne = temp1.charAt(0);
    
     //gets character to fill diamonds with from user
    System.out.print("Enter your second character to fill the diamonds with: ");
    String temp2 = myScanner.next();
    char fillTwo = temp2.charAt(0);
    
    //gets character to fill argyle stripe with from user
    System.out.print("Enter a character to fill the stripe with: ");
    String temp3 = myScanner.next();
    char fillStripe = temp3.charAt(0);
    
   
    for (int i = 1; i < heightWindow + 1 ; i++){ //sets height of viewing window
      for (int j = 1; j < widthWindow + 1; j++){ // sets width of veiwing window
        int k = (j-1) / sizeDiamonds; //creates another variable to simplify statements below
        if ((k+3) % 2 == 1 ){ //creates first patter
          if ((((sizeDiamonds - (j % sizeDiamonds) + i + 2) % sizeDiamonds) <= (stripeWidth )) && (((sizeDiamonds - (j % sizeDiamonds) + i + 2) % sizeDiamonds) > 0)){
          System.out.print(fillStripe); 
          }
          else if ((( j + sizeDiamonds) % sizeDiamonds) <= (sizeDiamonds - (i % sizeDiamonds)) && (j % sizeDiamonds != 0)){
          System.out.print(fillOne);
          }
          else{
          System.out.print(fillTwo);
          }
        }
        
        else if ((k +3) % 2 == 0){ //creates second pattern
          if ((((sizeDiamonds + (j % sizeDiamonds) + i + 2) % sizeDiamonds) <= (stripeWidth )) && (((sizeDiamonds + (j % sizeDiamonds) + i + 2) % sizeDiamonds) > 0)){
          System.out.print(fillStripe); 
          }
          else if (( j + sizeDiamonds) % sizeDiamonds <= i % sizeDiamonds && (j % sizeDiamonds != 0)){
          System.out.print(fillOne);
          }
          else{
            System.out.print(fillTwo);
          }
      }
      
    }
  }
    
    
}
}
=============================
grade6.txt
=============================
Grade: 90/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
The code compiles

B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
The code runs properly

C) How can any runtime errors be resolved?
N/A

D) What topics should the student study in order to avoid the errors they made in this homework?
N/A

E) Other comments:
You almost got it, but the output pattern is wrong. Great job otherwise!
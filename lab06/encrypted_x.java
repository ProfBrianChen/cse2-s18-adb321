// Amanda Baran March 9 2018
//CSE 2 - Lab 06
// Develop a problem to print out the hidden message X

import java.util.Scanner; //imports scanner

public class encrypted_x{ //beginning of class
  
  public static void main(String[] args){ //beginning of main method
    
    Scanner myScanner = new Scanner(System.in); //creates scanner
    
    int size = 1; //intializes size of square
    
    do {System.out.print("Enter an integer between 0 and 100: ");
        while (!myScanner.hasNextInt()){
          String junkWord = myScanner.next();
          System.out.print("Enter an integer between 0 and 100: ");
        } 
        size = myScanner.nextInt();
       }
    while (size < 0 || size > 100); 
    // prompts user for an integeger between 0 and 100 that represents the size of the square
    
    for (int i = 0; i < size; i++){ //rows
      for (int j = 0; j < size + 1; j++){ //collumns
        if (i == j || i == (size - j)){
          System.out.print(" "); //prints the hidden X
        }
        else {
          System.out.print("*"); //prints the * surrounding the hidden X
        }
      }
      System.out.println();
    }
      
    
  }
}

///Amanda Baran February 2 2018 CSE 2-210//
// Lab 02 // This program will create a bicycle cyclometer which will measure speed, distance, etc.
//
public class Cyclometer {
  // main method required for every Java program
  public static void main(String[] args) {
   // our input data
    int secsTrip1=480; // number of minutes for trip 1
    int secsTrip2=3220; // number of minutes for trip 2
    int countsTrip1=1561; // number of rotations of the front wheel for trip 1
    int countsTrip2=9037; // number of rotations of the front wheel for trip 2
    
    // our intermediate variables and output data
    double wheelDiameter=27.0, // diamter of bicycle wheel
    PI=3.14159, // numerical value of pi
    feetPerMile=5280, // how many feet there are in a mile
    inchesPerFoot=12, // how many inches there are in a foot
    secondsPerMinute=60;  //how many seconds there are in a minute
    double distanceTrip1, distanceTrip2, totalDistance; // total distance of both trips
    
    System.out.println("Trip 1 took " +
                      (secsTrip1/secondsPerMinute)+" minutes and had "+
                      countsTrip1+" counts.");
    System.out.println("Trip 2 took "+
                      (secsTrip2/secondsPerMinute)+" minutes and had "+
                      countsTrip2+" counts.");
    // 
    // Calculations for Trip 1 and Trip 2 measurements
    //
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    // Above gives distance in inches
    //(for each count, a rotation of the wheel travels
    //the diameter in inches times PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    
    // Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
    
    
  } //end of main method
} // end of class

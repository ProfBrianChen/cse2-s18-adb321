// Amanda Baran March 23 2018
// CSE 2 Lab 07-- Practice creating methods with artificial story generation.
//
import java.util.Random;

public class lab07{ //class
  
  public static void main(String[] args){ //main method
    
    String x = "abc";
    action(x);
    
    String y = "def";
    conclusion(y);
    
  }
  
  public static String adjectives(String adj){ //method with random adjectives
  
  Random randomGenerator = new Random();
  int randomInt = randomGenerator.nextInt(10);
  
    
  switch (randomInt){
    case 0:
      adj = "brown";
      break;
    case 1:
      adj = "red";
      break;
    case 2:
      adj = "agile";
      break;
    case 3:
      adj = "sluggish";
      break;
    case 4:
      adj = "swift";
      break;
    case 5:
      adj = "old";
      break;
    case 6:
      adj = "young";
      break;
    case 7: 
      adj = "wrinkled";
      break;
    case 8: 
      adj = "smooth";
      break;
    case 9:
      adj = "beautiful";
      break;      
  }
    return adj;
    
  }
  
  public static String subjects(String sub){ //method with random subject nouns
  
  Random randomGenerator = new Random(); 
  int randomInt = randomGenerator.nextInt(10);
   
    
  switch (randomInt){
    case 0:
      sub = "fox";
      break;
    case 1:
      sub = "turtle";
      break;
    case 2:
      sub = "girl";
      break;
    case 3:
      sub = "man";
      break;
    case 4:
      sub = "woman";
      break;
    case 5:
      sub = "worker";
      break;
    case 6:
      sub = "doctor";
      break;
    case 7: 
      sub = "teacher";
      break;
    case 8: 
      sub = "cat";
      break;
    case 9:
      sub = "dog";
      break;      
  }
    return sub;
    
 
  }
  
 public static String pastVerbs(String verb){ //method with random past tense verbs
  
  Random randomGenerator = new Random();
  int randomInt = randomGenerator.nextInt(10);
    
    
  switch (randomInt){
    case 0:
      verb = "ran";
      break;
    case 1:
      verb = "flew";
      break;
    case 2:
      verb = "jumped";
      break;
    case 3:
      verb = "bought";
      break;
    case 4:
      verb = "stole";
      break;
    case 5:
      verb = "ate";
      break;
    case 6:
      verb = "sat";
      break;
    case 7: 
      verb = "broke";
      break;
    case 8: 
      verb = "fought";
      break;
    case 9:
      verb = "cried";
      break;      
  }
    return verb;
    
  }
  
  public static String objects(String obj){ //method with random objects
  
  Random randomGenerator = new Random();
  int randomInt = randomGenerator.nextInt(10);
    
    
  switch (randomInt){
    case 0:
      obj = "chair";
      break;
    case 1:
      obj = "stapler";
      break;
    case 2:
      obj = "bike";
      break;
    case 3:
      obj = "table";
      break;
    case 4:
      obj = "plane";
      break;
    case 5:
      obj = "car";
      break;
    case 6:
      obj = "train";
      break;
    case 7: 
      obj = "pool";
      break;
    case 8: 
      obj = "window";
      break;
    case 9:
      obj = "clock";
      break;      
  }
    return obj;
   
  }
  
  public static String thesis( String mainSub){ //thesis statement method
   
    String adj = "abc";
    String obj = "def";
    String verb = "ghi";
    String sub = "employee";

    mainSub = subjects(sub);
    
    System.out.println("The " + adjectives(adj) + " " + adjectives(adj) + " " + mainSub + " " + pastVerbs(verb) + " the " + adjectives(adj) + " " + objects(obj) + ".");
    return mainSub;
  }
  
  public static String action(String x){ //action sentences method
    Random randomGenerator2 = new Random();
    int randomInt2 = randomGenerator2.nextInt(6) + 4;
    
    String adj = "abc";
    String obj = "def";
    String verb = "ghi";
    String sub = "jkl";
    String mainSub = "mno";
    
    String subjectNew = thesis(mainSub);
    int i = 1;
    
    while ( i < randomInt2 / 2){
      System.out.println("Then the " + subjectNew + " " + pastVerbs(verb) + " and " + pastVerbs(verb) + " the " + objects(obj) + ".");
      System.out.println("It " + pastVerbs(verb) + " the " + objects(obj) + " and " + pastVerbs(verb) + " the " + objects(obj) + ".");
      i++;
    }
    
    return subjectNew;
  }
  
  public static void conclusion(String y){ //conclusion method
    
    String adj = "abc";
    String obj = "def";
    String verb = "ghi";
    String sub = "jkl";
    String subjectNew = "mno"; 
    
    y = action(subjectNew);
    
    System.out.println("That " + y + " " + pastVerbs(verb) + " their " + objects(obj) + ".");
  }
    
}

// Amanda Baran February 16 2018 CSE 2
// Lab 4 - Create a program which generates a random card in order to practice magic tricks.
//
//


public class CardGenerator{
  //main method requireed for every java program
  public static void main(String[] args) {
    
    int randomNumber = (int)(Math.random()*52)+1;
    //generates a random number between 1 and 52
    
    String suit = "suit";
    String identity = "identity";
    //declares variables
    
    if (randomNumber <= 13){
     suit = "Diamonds";
    }
    else if ( randomNumber <= 26 ){
      suit = "Clubs";
    }
    else if (randomNumber <= 39 ){
      suit = "Hearts";
    }
    else if (randomNumber <= 52 ){
      suit = "Spades";
    }
    // Assigns each card a suit based on the random number generated.
    
    switch (randomNumber) {
      case 1:
      case 14:
      case 27:
      case 40:
        identity = "Ace";
        break;
      case 2:
      case 15:
      case 28:
      case 41:
        identity = "2";
        break;
      case 3:
      case 16:
      case 29:
      case 42:
        identity = "3";
        break;
      case 4:
      case 17:
      case 30:
      case 43:
        identity = "4";
        break;
      case 5:
      case 18:
      case 31:
      case 44:
        identity = "5";
        break;
      case 6:
      case 19:
      case 32:
      case 45:
        identity = "6";
        break;
      case 7:
      case 20:
      case 33:
      case 46:
        identity = "7";
        break;
      case 8:
      case 21:
      case 34:
      case 47:
        identity = "8";
        break;
      case 9:
      case 22:
      case 35:
      case 48:
        identity = "9";
        break;
      case 10:
      case 23:
      case 36:
      case 49:
        identity = "10";
        break;
      case 11:
      case 24:
      case 37:
      case 50:
        identity = "Jack";
        break;
      case 12:
      case 25:
      case 38:
      case 51:
        identity = "Queen";
        break;
      case 13:
      case 26:
      case 39:
      case 52:
        identity = "King";
        break;
           
    }//end of switch statements
    //assigns the identity of the card based on the random number
    
    System.out.println("The number you picked is " + randomNumber ); //prints out the randome number
    System.out.println("You picked the " + identity + " of " + suit); //prints out the card that was picked
        
    } //end of main method
      

  } //end of class
    